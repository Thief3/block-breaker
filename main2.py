### Blocks
###	A game by Malik Kissarli

import pygame, sys
from pygame.locals import *
from random import randint

# GAME SYSTEM

pygame.init()
fpsClock = pygame.time.Clock()
HORZ = 5
VERT = 15
C_HEIGHT = 30
C_WIDTH = 30
SIZE = (C_WIDTH , C_HEIGHT)
DSHEET = pygame.image.load("spritesheet1.png") # Destruction Animations
gameSurface = pygame.display.set_mode((C_WIDTH*HORZ, C_HEIGHT*VERT))
pygame.display.set_caption("Blocks")
score = 0
multiplier = 0

class ThemePack():
    """
    Represents all the icons to be used in a set, all of a theme's settings.
    
    # Variables

    "dSheet" is the sheet with the destruction animations.
    "dFrameNo" is the number of frames in the destruction animations.
    "dStart" is where the destruction animation starts as a tuple point.
    "iSheet" is the sheet with the idle animations.
    "iFrameNo" is the number of frames in the idle animations.
    "iStart" is where the idle animation starts as a tuple point.
    "colourList" contains a list of all the different cubes in the theme.
    "packNo" is the place the theme pack has in the sprite sheets, starting 0.
    specialEffects is undecided at the moment.
    """

    def __init__(self, dSheet, dFrameNo, dStart, iSheet, iFrameNo, iStart, packNo):
	self.dSheet = cutSheet(dSheet, dFrameNo, (210*packNo, dStart))
	self.iSheet = cutSheet(iSheet, iFrameNo, (210*packNo, iStart))
	self.iFrameNo = iFrameNo
	self.colourList = []
	for i in range(7):
	    self.colourList.append((ColourType(self.dSheet, dFrameNo, self.iSheet, iFrameNo, i)))

class ColourType():
    """
    Holds animation frames.

    # Variables
    
    "regular" is the ColourType's still image.
    "destruction" is the ColourType's destruction animation split into frames
    in an array.
    "idle" is the ColourType's idle animation split into frames in an array.
    "x" is the place of the ColourType in the set.
    """

    def __init__(self, dSheet, dFrameNo, iSheet, iFrameNo, x):
	self.xStart = x * 30
	self.regular = dSheet.subsurface((self.xStart, 0), SIZE)
	self.dAnimation = []
	self.iAnimation = []

	for i in range(dFrameNo):
	    self.dAnimation.append(dSheet.subsurface((self.xStart, i*C_HEIGHT), SIZE))

	for i in range(iFrameNo):
	    self.iAnimation.append(iSheet.subsurface((self.xStart, i*C_HEIGHT), SIZE))

class Block():
    pass

class SpecialEffects():
    pass

def cutSheet(sheet, frameNo, xStart):
    """ 
    Returns a cut of the sheet, with all the animations from that set only.
    "sheet" is an image.
    "frameNo" is the number of frames.
    "xStart" is the starting tuple of the pack.
    """
    return sheet.subsurface(xStart, (210, (frameNo * C_HEIGHT)))

def destruction(x, y, colour):
    pass

def replace(x, y):
    pass

def reset():
    pass

def soloBlock(x, y):
    pass

gameSurface.fill((0, 0, 160))
crystalPack = ThemePack(DSHEET, 30, 0, DSHEET, 30, 0, 1)

while True:
    for i in crystalPack.colourList[1].dAnimation:
	gameSurface.fill((0,0,160), ((0,0), SIZE))
	gameSurface.blit(i, ((0, 0), SIZE))
	pygame.display.flip()
	pygame.time.delay(50)

    gameSurface.blit(crystalPack.colourList[3].regular, ((30,30), SIZE))
    pygame.display.flip()
    pygame.display.update()

    fpsClock.tick(30)
