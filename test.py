import pygame, sys, random
from pygame.locals import *

# GAME INFO

pygame.init()
fpsClock = pygame.time.Clock()
HORIZONTAL = 5
VERTICAL = 15
C_HEIGHT = 32
C_WIDTH = 32
SPRITESHEET = pygame.image.load("spritesheet1.png")
gameSurface = pygame.display.set_mode((C_WIDTH*HORIZONTAL, C_HEIGHT*VERTICAL))
pygame.display.set_caption("The Block Breaker")


class Block():
    def __init__(self, colour, specialEffect):
        self.draw = colour
        self.specialEffect = specialEffect

class Colour():
    def __init__(self, destructionSheet, xStart, yStart, frames):
        self.destructionSheet = destructionSheet
        self.xStart = xStart
        self.yStart = yStart
	self.frames = frames

    def destruction(self, mainX, mainY):
        for y in range(self.frames):
            gameSurface.blit(self.destructionSheet,(mainX,mainY), (self.xStart,self.yStart*2,C_HEIGHT,C_WIDTH))
            pygame.display.flip()
	    pygame.time.delay(30)
	    print(y)
	    
    def still(self, mainX, mainY):
	gameSurface.blit(self.destructionSheet, (mainX, mainY), (self.xStart, self.yStart,C_HEIGHT,C_WIDTH))
	pygame.display.flip()

class ThemePack():
    def __init__(self, destructionSheet, frames, packNo, yStart):
        self.destructionSheet = destructionSheet
        self.frames = frames
        self.packNo = packNo 
        self.yStart = yStart
	self.colourList = []
        for i in range(7):
            self.colourList.append((Colour(self.destructionSheet, i*30+packNo*210, self.yStart, self.frames)))
	#print(self.colourList)
crystalPack = ThemePack(SPRITESHEET, 27, 0, 90)
evilFacesPack = ThemePack(SPRITESHEET, 30, 1, 0)
themePackInUse = crystalPack

#field = [[themePackInUse.colourList[random.randint(0, 5)] for i in range(HORIZONTAL)] for j in range(VERTICAL)]

field = [[Block(themePackInUse.colourList[random.randint(0,6)],0) for i in range(HORIZONTAL)] for j in range(VERTICAL)]

gameSurface.fill((0,0, 160))

for h in range(HORIZONTAL):
    for v in range(VERTICAL):
	field[v][h].draw.still((h*C_WIDTH),(v*C_HEIGHT))

while True:
   # field[10][4].draw.destruction(300,120)
    pygame.display.update()
    fpsClock.tick(30)
