import pygame, sys, random
from pygame.locals import *

# GAME INFO

pygame.init()
fpsClock = pygame.time.Clock()
HORIZONTAL = 5
VERTICAL = 15
CUBEHEIGHT = 32
CUBEWIDTH = 32
SPRITESHEET = pygame.image.load("./spritesheet1.png")
gameSurface = pygame.display.set_mode((CUBEWIDTH*HORIZONTAL, CUBEHEIGHT*VERTICAL))
pygame.display.set_caption("The Block Breaker")

# VARIABLE SAFE
score = 0
multiplier = 0

## Random creation
field = [[colours[random.randint(0, 6)] for i in range(HORIZONTAL)] for j in range(VERTICAL)]
## Field is field[VERTICAL LAYER][HORIZONTAL LAYER]

class Block():
    def __init__(self, colour, specialEffect):
	self.colour = colour
	self.specialEffect = specialEffect

    def draw():
	self.colour.drawDestruction(mainX,mainY)

class Colour():
    def __init__(self, destructionSheet, xStart, yStart, frames):
	self.destructionSheet = destructionSheet
	self.xStart = xStart
	self.yStart = yStart

    def drawDestruction(mainX, mainY):
	for y in range(frames):
	    gameSurface.blit(self.image,(mainX,mainY), (self.xStart,self.yStart*y,C_HEIGHT,C_WIDTH))
	    gameSurface.display.flip()

class ThemePack():
    def __init__(self, destructionSheet, frames, packNo, yStart):
    	self.destructionSheet = destructionSheet
	self.frames = frames
	self.packNo = packNo
	self.colourList = []
	for i in range(6):
	    self.colourList.append((new Colour(self.destructionSheet, (self.packNo*C_WIDTH*(5+i)), self.yStart, self.frames)))    

def destruction(x, y, colour):
    if field[x][y] == EMPTY:
	return 0
    elif field[x][y] != colour:
	return 0
    else:
	cubeVal = 0 
	field[x][y] = EMPTY

	for z in [(-1,0),(0,1),(0,-1),(1,0)]:
	    i, j = z
	    if (i+x) >= 0 and (j+y) >=0 and (i+x) <= VERTICAL-1 and (j+y) <= HORIZONTAL-1:
		if field[x+i][y+j] == colour:
		    cubeVal += 1 + destruction((x+i), (y+j), colour)
	return cubeVal

def replace(y,x):
    if y-1 >= 0:
	field[y][x] = field[y-1][x]
	replace(y-1,x)
    else:
	field[y][x] = EMPTY
def reset():
    for i in range(VERTICAL):
	for j in range(HORIZONTAL):
	    if field[i][j] == EMPTY:
		replace(i,j)

def soloBlock(y, x):
    soloBlock = True
    for z in [(-1,0),(0,1),(0,-1),(1,0)]:
	i, j = z
	if (i+x) >= 0 and (j+y) >=0 and (i+x) <= VERTICAL-1 and (j+y) <= HORIZONTAL-1:
	    if field[x][y] == field[x+i][y+j]:
		soloBlock = False
    return soloBlock

while True:
    for h in range(HORIZONTAL):
	for v in range(VERTICAL):
	    pygame.draw.rect(gameSurface, field[v][h], (((CUBEWIDTH*h),(CUBEHEIGHT*v)),(30,30)))    
 
    for event in pygame.event.get():
	if event.type == QUIT:
	    pygame.quit()
	    sys.exit()
	if event.type == KEYUP:
	    if event.key == K_ESCAPE:
		pygame.quit()
		sys.exit()
	if event.type == MOUSEBUTTONUP:
	    if event.button == 1:
		y, x = event.pos ##MAGIC DON'T TOUCH...Honestly no idea why this works even tho event.pos returns a tuple of (x,y) ..... Think on later...
		y = y//32
		x = x//32
		#field[y][x] = DEBUG
		
		if soloBlock(y, x) == False:
		    multiplier = 1 +  destruction(x, y, field[x][y])
		    score = score + 2 * multiplier + multiplier
		print("Mutliplier: " + str(multiplier))
		print("Score: " + str(score))
    reset()

    pygame.display.update()
    fpsClock.tick(30)

## Every block is 32*32 pixels. Click, if in boundaries of game, divide to find block clicked, then run maths on arra
